package com.example.demo.service;

import com.example.demo.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository = new UserRepository();

    public int findUserIdByLogin(String login) {
        return this.userRepository.findUserIdByLogin(login);
    }
    public boolean checkLogin(int user_num, String password) {
        return this.userRepository.checkLogin(user_num, password);
    }
    public boolean isActive(int user_num) {
        return this.userRepository.isActive(user_num);
    }

}
